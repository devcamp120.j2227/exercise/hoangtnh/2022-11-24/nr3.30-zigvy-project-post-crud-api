//import thư viện mongoose
const mongoose = require("mongoose");
//import post model
const postModel = require("../models/postModel");
const { post } = require("../routes/postRouter");

//function create Post
const createPost = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let body = request.body;

    //B2: validate dữ liệu
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    }
    if(!body.body){
        return response.status(400).json({
            status:"Bad request",
            message:"Body is not valid"
        })
    }

    //B3: thao tác với CSDL
    let newPost = {
        _id: mongoose.Types.ObjectId(),
        userId : mongoose.Types.ObjectId(),
        title: body.title,
        body: body.body
    }
    if(body.title !== undefined){
        newPost.title = body.title
    }
    if(body.body !== undefined){
        newPost.body = body.body
    }
    postModel.create(newPost, (error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new post successfully",
            data: data
        })
    })
}
//function get all post
const getAllPost = (request, response) => {
    postModel.find((error,data) =>{
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:" Get all posts successfully",
            data: data
        })
    })
}
//function update post by id
const updatePost = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let postId = request.params.postId;
    let body = request.body

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return response.status(400).json({
            status:"Bad request",
            message:" Post id is not valid"
        })
    }
    if(!body.title ){
        return response.status(400).json({
            status:"Bad request",
            message:"Title is not valid"
        })
    }
    if(!body.body){
        return response.status(400).json({
            status:"Bad request",
            message:"Body is not valid"
        })
    }
    //B3: gọi model thao tác CSDL
    const postUpdate = {};
    if(body.title !== undefined){
        postUpdate.title = body.title
    }
    if(body.body !== undefined){
        postUpdate.body = body.body
    }
    postModel.findByIdAndUpdate(postId, postUpdate, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:`Update post with id${postId} successfully`,
            data: data
        })
    })
}
//function get post by id
const getPostById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let postId = request.params.postId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return response.status(400).json({
            status:"Bad request",
            message:"Post id is not valid"
        })
    }
    //B3: gọi model chứa id thao tác với csdl
    postModel.findById(postId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get post by id ${postId} successfully`,
            data: data
        })
    })
}
//function delete post by id
const deletePost = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let postId = request.params.postId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId )){
        return response.status(400).json({
            status:"Bad request",
            message: "Post id is not valid"
        })
    }
    //B3: thao tác với CSDL
    postModel.findByIdAndRemove(postId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Deleted post by id ${postId} successfully`
        })
    })
}
module.exports = {
    createPost,
    getAllPost,
    updatePost,
    getPostById,
    deletePost
}